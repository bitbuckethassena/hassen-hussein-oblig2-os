# Obligatorisk oppgave 2 - Bash pipelines og scripting

Denne oppgaven best�r av de f�lgende laboppgavene fra kompendiet:

* 7.5.a (Prosesser og tr�der)
* 8.6.c (Page faults)
* 8.6.d (En prosess sin bruk av virtuelt og fysisk minne)
* 9.4.a (Informasjon om deler av filsystemet)

SE OPPGAVETEKST I KOMPENDIET. HUSK � REDIGER TEKSTEN NEDENFOR!

Pipelinen returnerer ikke suksessful, blant annet med cat i i 8.6.d ber meg bruke cmd> file, men da f�r jeg ikke riktitge output og har derfor valgt � g� med cat. 
Vi har noe kvalitetsikring med blant annet fsinfo, som � skrive ut feilmelding om et ikke eksisterende directory ble gitt som argument.
har ogs� jobbet med andre med � l�se denne oppgaven Madhushan, Mohammed og Abbirami alts� itsm gruppa v�r, s� derfor kan noen av besvarlesene se like ut. 

## Gruppemedlemmer

**TODO: Erstatt med navn p� gruppemedlemmene**

Hassen Afzal
Hussein Afzal

## Sjekkliste

* Har navnene p� gruppemedlemmene blitt skrevet inn over?
* Har l�ringsassistenter og foreleser blitt lagt til med leserettigheter?
* Er issue-tracker aktivert?
* Er pipeline aktivert, og returnerer pipelinen "Successful"?
