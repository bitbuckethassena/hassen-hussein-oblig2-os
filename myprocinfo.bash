#!/bin/bash 

clear 
while [ "$svar" != "9" ]
do
echo ""
echo " 1 - hvem er jeg?"
echo " 2 - Hvor lenge er det siden siste boot?"
echo " 3 - Hva var gjennomsnittlig load siste minutt?"
echo " 4 - Hvor mange prosesser og tråder finnes?"
echo " 5 - Hvor mange context switch'er fant sted siste sekund?"
echo " 6 - Hvor mange interrupts fant sted siste sekund?"
echo " 9 -Avslutt dette scriptet"
echo ""
echo -n "Velg en funksjon: "
read -r svar
case $svar in
1)clear
echo "Ja hvem er jeg?"
read -r jeg 
echo "Jeg er: $jeg"
;;
2) 
echo "Tid siden siste boot?"
last reboot
;;
3)
echo -n "siste minutts load var: "
uptime
;;
4) 
echo "Det finnes så mange prosesser: "
nproc
echo "Det finnes så mange tråder: "
ps -aux|wc -l
;;
5)
echo "Det fantes følgende context swither i siste sekund:"
vmstat -s|grep "CPU context switches"
;;
6)
echo "Det var følgene antall interrupts siste sekund: "
cat /proc/interrupts
;;
9)
echo "Scriptet avsluttes!! "
exit
;;
esac
done

