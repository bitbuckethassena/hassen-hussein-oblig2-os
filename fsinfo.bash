#!/bin/bash
#scriptet tar en directory som argument og skriver

if [[ -d $1 ]]; 
then

echo "vi er i $1 directory"

links=$(find "$1" -depth -type f -print0 | xargs --null ls -la | awk '{print $2 , $9}' | sort | tail -n 1)

echo "Partition $1 er: $(df -h "$1" | awk '{print $5}' ) full"
echo "Det finnes: $(find "$1" | wc -l) filer"
echo "Den storste er $(find "$1" -depth -type f | tr '\n' '\0' | du -ah --files0-from=- | sort -h | tail -n 1)"
echo "Gjennomsnittlig filstørrelse er ca. $(find "$1" -depth -type f | tr '\n' '\0' | du -a --files0-from=- | awk '{print $1}' | sed ':a;N;$!ba;s/\n/+/g' | bc) bytes"
echo "Filen: $(echo "$links" | awk '{print $2}')  har flest hardlinks, den har $(echo "$links" | awk '{print $1}')"
 else
echo "$1 is not a directoy "

fi
