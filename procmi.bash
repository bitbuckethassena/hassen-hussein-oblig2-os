#!/bin/bash

#echo Eksempel på datoformat: $(date +%Y%m%d-%H:%M:%S)"


for pid in "$@"; do
	echo "******** Minne info om prosess med PID $pid *******" >> "$pid-$(date +%Y%m%d-%H:%M:%S).meminfo"
	VMS=$(cat /proc/"$pid"/status | grep VmSize | awk '{print $2}')
	echo "Total bruk av virtuelt minne" "(VmSize):" "$VMS""KB"  >> "$pid-$(date +%Y%m%d-%H:%M:%S).meminfo"
	k=$(cat /proc/"$pid"/status | grep VmData | awk '{print $2}')
	b=$(cat /proc/"$pid"/status | grep VmStk | awk '{print $2}')
	c=$(cat /proc/"$pid"/status | grep VmExe | awk '{print $2}')
	a=$((k+b+c))
	echo "Mengde privat virtuelt minne" "(VmData + VmStk + VmExe):" "$a""KB"  >> "$pid-$(date +%Y%m%d-%H:%M:%S).meminfo"
	VML=$(cat /proc/"$pid"/status | grep VmLib | awk '{print $2}')
	echo "Mengde sharedvirtuelt minne" "(VmLib):" "$VML""KB"  >> "$pid-$(date +%Y%m%d-%H:%M:%S).meminfo"
	VMR=$(cat /proc/"$pid"/status | grep VmRSS | awk '{print $2}')
	echo "Total bruk av fysisk minne" "(VmRSS):" "$VMR""KB"  >> "$pid-$(date +%Y%m%d-%H:%M:%S).meminfo"
	VMP=$(cat /proc/"$pid"/status | grep VmPTE | awk '{print $2}')
	echo "Mengde fysisk minnne som benyttes til page table" "(VmPTE):" "$VMP""KB"  >> "$pid-$(date +%Y%m%d-%H:%M:%S).meminfo"
done
