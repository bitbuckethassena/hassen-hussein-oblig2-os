#!/bin/bash
for i in $( pgrep chrome )
do
echo  "Chrome $i har forårsaket $(ps --no-headers -o maj_flt "$i") major page faults"
if [ "$(ps --no-headers -o maj_flt "$i")" -gt 1000 ];
then echo " over 1000 major page faults!!"
fi
done
